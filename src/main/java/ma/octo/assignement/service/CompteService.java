package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;

import java.util.List;

public interface CompteService {
    Compte storeCompte(Compte compte);
    List<Compte> allComptes();
}
