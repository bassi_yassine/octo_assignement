package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementServiceImpl implements VersementService {

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private AuditService auditService;

    @Override
    public void CreateTransaction(VersementDto versementDto)
        throws CompteNonExistantException, TransactionException {

        Compte compteBeneficiaire = compteRepository.findCompteByRib(versementDto.getRib());
        if (compteBeneficiaire == null) throw new CompteNonExistantException("Compte Non existant");

        if (versementDto.getMontantVersement() == null || versementDto.getMontantVersement().intValue() == 0) throw new TransactionException("Montant vide");
        if (versementDto.getMontantVersement().intValue() < 10) throw new TransactionException("Montant minimal de virement non atteint");

        if (versementDto.getMotif().length() == 0) throw new TransactionException("Motif vide");
        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Versement versement = new Versement(null, versementDto.getMontantVersement(), versementDto.getDate(), versementDto.getNom_emeteur(), compteBeneficiaire, versementDto.getMotif());

        versementRepository.save(versement);

        auditService.auditVersement("Versement depuis " + versementDto.getNom_emeteur() + " vers " + compteBeneficiaire.getNrCompte()
                + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
    }

    @Override
    public List<Versement> allVersements() {
        return versementRepository.findAll();
    }
}
