package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class VirementServiceImpl implements VirementService {


    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);


    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private AuditService auditService;

    @Override
    public List<Virement> allVirement() {
        return virementRepository.findAll();

    }

    @Override
    public void createTransaction(VirementDto virementDto) throws CompteNonExistantException, TransactionException {

        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (compteBeneficiaire == null || compteEmetteur == null) throw new CompteNonExistantException("Compte Non existant");

        if (virementDto.getMontantVirement() == null || virementDto.getMontantVirement().intValue() == 0) throw new TransactionException("Montant vide");
        else if (virementDto.getMontantVirement().intValue() < 10) throw new TransactionException("Montant minimal de virement non atteint");
        else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) throw new TransactionException("Montant maximal de virement dépassé");

        if (virementDto.getMotif().length() == 0) throw new TransactionException("Motif vide");

        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) LOGGER.error("Solde insuffisant pour l'utilisateur");

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Virement virement = new Virement(null, virementDto.getMontantVirement(), virementDto.getDate(), compteEmetteur, compteBeneficiaire, virementDto.getMotif());

        virementRepository.save(virement);

        auditService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString());
    }

}
