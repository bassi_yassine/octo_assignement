package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface VersementService {
    void CreateTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException;
    List<Versement> allVersements();
}
