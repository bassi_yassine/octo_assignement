package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.util.EventType;

public interface AuditService {
    void auditVirement(String message);
    void auditVersement(String message);
}
