package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
public class UtilisateurController {

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping("lister-utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.allUtilisateurs();
    }
}
