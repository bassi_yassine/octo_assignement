package ma.octo.assignement.web;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
public class VersementController {

    @Autowired
    VersementService versementService;

    @GetMapping("lister-versements")
    List<Versement> allVersement() {
        return versementService.allVersements();
    }

    @PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto) throws TransactionException, CompteNonExistantException {
        versementService.CreateTransaction(versementDto);
    }
}
