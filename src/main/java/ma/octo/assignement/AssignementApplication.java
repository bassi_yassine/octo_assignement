package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.RoleRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurService utilisateurService;
	@Autowired
	private VirementRepository virementRepository;

	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		Utilisateur utilisateur1 = utilisateurService.storeUtilisateur(new Utilisateur(null, "user1", "1234", new ArrayList<>(),"last1", "first1", "Male", null));
		Utilisateur utilisateur2 = utilisateurService.storeUtilisateur(new Utilisateur(null, "user2", "1234", new ArrayList<>(), "last2", "first2", "Female", null));

		Compte compte1 = compteRepository.save(new Compte(null, "010000A000001000", "RIB1", BigDecimal.valueOf(200000L), utilisateur1));;
		Compte compte2 = compteRepository.save(new Compte(null, "010000B025001000", "RIB2", BigDecimal.valueOf(140000L), utilisateur2));

		virementRepository.save(new Virement(null, BigDecimal.TEN, new Date(), compte2, compte1, "Assignment 2021"));
	}
}