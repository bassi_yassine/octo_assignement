package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class VersementDto {
    private String rib;
    private String nom_emeteur;
    private BigDecimal montantVersement;
    private Date date;
    private String motif;
}
