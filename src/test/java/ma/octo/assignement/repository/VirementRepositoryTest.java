package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.service.CompteService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DataJpaTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;
  @Autowired
  private CompteService compteService;
  @Autowired
  private UtilisateurRepository utilisateurRepository;

  private Virement virement;

  @BeforeEach
  public void setup(){
    Utilisateur utilisateur1 = utilisateurRepository.save(
            new Utilisateur(null, "user1", "1234", new ArrayList<>(),"last1", "first1", "Male", null));
    Utilisateur utilisateur2 = utilisateurRepository.save(
            new Utilisateur(null, "user2", "1234", new ArrayList<>(), "last2", "first2", "Female", null));

    Compte compte1 = compteService.storeCompte(new Compte(null, "010000A000001000", "RIB1", BigDecimal.valueOf(200000L), utilisateur1));;
    Compte compte2 = compteService.storeCompte(new Compte(null, "010000B025001000", "RIB2", BigDecimal.valueOf(140000L), utilisateur2));

    virement = new Virement(null, new BigDecimal(800), new Date(), compte2, compte1, "Assignment 2021");
    virementRepository.save(virement);

  }

  @Test
  public void findAll() {
    List<Virement> virements = virementRepository.findAll();
    Assertions.assertThat(virements.size()).isGreaterThan(0);
  }

  @Test
  public void save() {
    Assertions.assertThat(virement.getId()).isGreaterThan(0);
  }


  @Test
  public void findOne() {
    Assertions.assertThat(virementRepository.findById(virement.getId()).get()).isEqualTo(virement);
  }

  @Test
  public void delete() {
    virementRepository.delete(virement);
    Assertions.assertThat(virementRepository.findById(virement.getId()).isPresent()).isFalse();
  }
}