package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.service.CompteService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DataJpaTest
@Transactional
public class VersementRepositoryTest {

    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    private Versement versement;

    @BeforeEach
    public void setup(){
        Utilisateur utilisateur1 = utilisateurRepository.save(
                new Utilisateur(null, "user1", "1234", new ArrayList<>(),"last1", "first1", "Male", null));

        Compte compte = compteService.storeCompte(new Compte(null, "010000A000001000", "RIB1", BigDecimal.valueOf(200000L), utilisateur1));;

        versement = new Versement(null, new BigDecimal(800), new Date(), "yassine", compte, "Assignment 2021");
        versementRepository.save(versement);

    }

    @Test
    public void findAll() {
        List<Versement> virements = versementRepository.findAll();
        Assertions.assertThat(virements.size()).isGreaterThan(0);
    }

    @Test
    public void save() {
        Assertions.assertThat(versement.getId()).isGreaterThan(0);
    }


    @Test
    public void findOne() {
        Assertions.assertThat(versementRepository.findById(versement.getId()).get()).isEqualTo(versement);
    }

    @Test
    public void delete() {
        versementRepository.delete(versement);
        Assertions.assertThat(versementRepository.findById(versement.getId()).isPresent()).isFalse();
    }
}
